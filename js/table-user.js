const dataTableUser = (data,val) => {
    let rowData = "";
    if(val === true){
      for (let i = 0; i < data.length; data++) {
        data.forEach((d) => {
          let isi = "";
          d.forEach((val) => {
            isi += `<td>${val}</td>`;
          });
          rowData += `<tr>${isi}</tr>`;
        });
      }
      document.getElementById("body-table").innerHTML = rowData;
      document.getElementById("table-loading").style.display = "none";
    }
    
  };
  const headerTableUser = (data) => {
    let isi = "";
    data.forEach((d) => {
      isi += `<th>${d}</th>`;
    });
    document.getElementById("table").innerHTML = `<table class="table table-bordered">
      <thead><tr>${isi}</tr></thead>
      <tbody id="body-table"></tbody>
    </table><p class='text-center mt-2 fs-5' id='table-loading'>Loading...</p>`;
  };

  

  export {dataTableUser, headerTableUser}
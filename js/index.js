import { headerTableUser, dataTableUser } from "./table-user.js";

const url = "https://jsonplaceholder.typicode.com/users";

function createTable(url, cb) {
  //header table
  let headerTable = ["id", "name", "username", "email", "city", "website"];
  headerTableUser(headerTable);
  const xhttp = new XMLHttpRequest();
  xhttp.onload = () => {
    if (xhttp.status === 200) {
      cb(JSON.parse(xhttp.responseText));
      console.log(JSON.parse(xhttp.responseText));
    } else {
      console.log("error " + xhttp.status);
    }
  };
  xhttp.open("GET", url);
  xhttp.send();
}
const cb = (data) => {
  let dataUser = [];
  data.forEach((d) => {
    dataUser.push([
      d.id,
      d.name,
      d.username,
      d.email,
      d.address.city,
      d.website,
    ]);
  });
  dataTableUser(dataUser, true);
};

createTable(url, cb);
